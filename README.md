# But

Permettre l'échange de copies entre enseignants et élèves sans perte de temps

# Moyen

* L'enseignant crée un devoir à rendre avec une dead line et reçoit un ID du devoir et un code de retour
* L'enseignant fournit un ID à ses élèves
* Les élèves déposent leurs copies sur la plate-forme pour un devoir donné en fournissant une adresse mail de retour
  * cette adresse est vérifiée (au premier dépôt un code envoyé par mail est demandé)
* À la date limite du devoir
  * le système concatène les copies en un paquet (un unique pdf)
  * envoie une notification à l'enseignant avec un lien sur le paquet
* Après correction (avec un outil du genre xournal) l'enseignant remet le paquet corrigé avec son code de retour
* Le système ventile les copies et envoie des nofications aux élèves avec un lien sur leurs copies
  * Une fois rendues les copies sont détruites

Problème: Quand faut-il détruire les copies ?

# Versions prévues

* v1. Sur plate-forme en ligne avec notifications contenant des liens vers les documents
* v2. Réception possible de mail (pour la création de devoirs par l'enseignant et l'envoi de copies par les élèves)
